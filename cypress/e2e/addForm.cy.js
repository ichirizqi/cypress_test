import addForm from "../support/pageObject/addForm"
const formData = require ('../fixtures/formData.json')

  describe('Input Review', () => {
    beforeEach(() => {
      cy.visit('')
    })
  
    it('Success Submit', () => {
        cy.get(addForm.full_name).type(formData["full-name"])
        addForm.verifyNameValue()
        cy.get(addForm.phone_number).type(formData["phone-number"])
        addForm.verifyNumberValue()
        cy.get(addForm.review).not('[disabled]').check()
        addForm.verifyReviewChecked()
        addForm.scrollPage()
        cy.get(addForm.rating_label).should('be.visible')
        addForm.clickRating()
        addForm.clickDateLogo()
        addForm.clickDate()
        addForm.clickSubmit()  
        addForm.successSubmit() 
    })

    it('Success Submit - Select Other Review', () => {
        cy.get(addForm.full_name).type(formData["full-name"])
        addForm.verifyNameValue()
        cy.get(addForm.phone_number).type(formData["phone-number"])
        addForm.verifyNumberValue()
        cy.get(addForm.other_review).not('[disabled]').check()
        addForm.verifyOtherReviewChecked()
        addForm.typeOtherReview()
        addForm.scrollPage()
        cy.get(addForm.rating_label).should('be.visible')
        addForm.clickRating()
        addForm.clickDateLogo()
        addForm.clickDate()
        addForm.clickSubmit() 
        addForm.successSubmit()     
    })

    it('Failed Submit - Blank All Input Field', () => {
        addForm.clickSubmit()
        addForm.scrollPage()
        addForm.verifyErrAll()
    })

    it('Failed Submit - Blank Full Name', () => {
        cy.get(addForm.phone_number).type(formData["phone-number"])
        addForm.verifyNumberValue()
        cy.get(addForm.review).not('[disabled]').check()
        addForm.verifyReviewChecked()
        addForm.scrollPage()
        cy.get(addForm.rating_label).should('be.visible')
        addForm.clickRating()
        addForm.clickDateLogo()
        addForm.clickDate()
        addForm.clickSubmit()        
        addForm.verifyErr()
    })

    it('Failed Submit - Blank Phone Number', () => {
        cy.get(addForm.full_name).type(formData["full-name"])
        addForm.verifyNameValue()
        cy.get(addForm.review).not('[disabled]').check()
        addForm.verifyReviewChecked()
        addForm.scrollPage()
        cy.get(addForm.rating_label).should('be.visible')
        addForm.clickRating()
        addForm.clickDateLogo()
        addForm.clickDate()
        addForm.clickSubmit()        
        addForm.verifyErr()
  })

    it('Failed Submit - Blank Review', () => {
        cy.get(addForm.full_name).type(formData["full-name"])
        addForm.verifyNameValue()
        cy.get(addForm.phone_number).type(formData["phone-number"])
        addForm.verifyNumberValue()
        addForm.scrollPage()
        cy.get(addForm.rating_label).should('be.visible')
        addForm.clickRating()
        addForm.clickDateLogo()
        addForm.clickDate()
        addForm.clickSubmit()
        addForm.verifyErr()
    })

    it('Failed Submit - Blank Rating', () => {
        cy.get(addForm.full_name).type(formData["full-name"])
        addForm.verifyNameValue()
        cy.get(addForm.phone_number).type(formData["phone-number"])
        addForm.verifyNumberValue()
        cy.get(addForm.review).not('[disabled]').check()
        addForm.verifyReviewChecked()
        addForm.scrollPage()
        cy.get(addForm.rating_label).should('be.visible')
        addForm.clickDateLogo()
        addForm.clickDate()
        addForm.clickSubmit()
        addForm.verifyErr()
    })

    it('Failed Submit - Blank Date', () => {
        cy.get(addForm.full_name).type(formData["full-name"])
        addForm.verifyNameValue()
        cy.get(addForm.phone_number).type(formData["phone-number"])
        addForm.verifyNumberValue()
        cy.get(addForm.review).not('[disabled]').check()
        addForm.verifyReviewChecked()
        addForm.scrollPage()
        cy.get(addForm.rating_label).should('be.visible')
        addForm.clickRating()
        addForm.clickSubmit()
        addForm.verifyErr()
    })
  })

  describe('After Submit Review', () => {
    beforeEach(() => {
        cy.visit('')
        cy.get(addForm.full_name).type(formData["full-name"])
        addForm.verifyNameValue()
        cy.get(addForm.phone_number).type(formData["phone-number"])
        addForm.verifyNumberValue()
        cy.get(addForm.review).not('[disabled]').check()
        addForm.verifyReviewChecked()
        addForm.scrollPage()
        cy.get(addForm.rating_label).should('be.visible')
        addForm.clickRating()
        addForm.clickDateLogo()
        addForm.clickDate()
        addForm.clickSubmit()    
      })
  
    it('Submit Another Response', () => {
        addForm.submitOtherResponse()
        addForm.verifySubmitOtherResponse()
    })

    it('Save Respone', () => {
        cy.get('[aria-label="Save my response"]').click()
    })

  })
  