const formData = require ('../../fixtures/formData.json')

class addForm{
    full_name = ':nth-child(1) > .-bY-49 > .-a-60 > .-mZ-65 > .-ar-64'
    phone_number = ':nth-child(2) > .-bY-49 > .-a-60 > .-mZ-65 > .-ar-64'
    review = ':nth-child(1) > div > .--n-81 > .-a-82 > .-gT-83'
    other_review = ':nth-child(2) > div > .--n-81 > .-a-82 > .-gT-83'
    other_review_value = '.-mZ-88 > .-ar-64'
    rating_label = '#QuestionId_r9f97f2550332479a8fdd2914bd99bc1d > .-Q-54 > .-q-55 > [aria-level="2"] > .text-format-content > span'
    rating = '[aria-label="3 Star"]'
    date_logo = '.ms-DatePicker-event--without-label'
    date_picker = '[aria-label="3, June, 2024"]'
    btn_submit = '[data-automation-id="submitButton"]'

    success = "Submit another response"
    verify_success = "Review our product"
    err_all = '5 question(s) need to be completed before submitting: Question 1,Question 2,Question 3,Question 4,Question 5.'
    err = 'This question is required.'
    save_response = "Save my response"
    review_title = "Review our product"

    verifyNameValue(){
        cy.get(this.full_name).should('have.value', formData["full-name"])
    }

    verifyNumberValue(){
        cy.get(this.phone_number).should('have.value', formData["phone-number"])
    }

    verifyReviewChecked(){
        cy.get(this.review).not('[disabled]').should('be.checked')
    }

    verifyOtherReviewChecked(){
        cy.get(this.other_review).not('[disabled]').should('be.checked')
    }

    typeOtherReview(){
        cy.get(this.other_review_value).type(formData["review_value"])
    }

    scrollPage(){
        cy.get(this.rating_label).scrollIntoView()
    }

    clickRating(){
        cy.get(this.rating).click()
    }

    clickDateLogo(){
        cy.get(this.date_logo).click()
    }

    clickDate(){
        cy.get(this.date_picker).click({force: true})
    }

    clickSubmit(){
        cy.get(this.btn_submit).click()
    }

    successSubmit(){
        cy.contains(this.success).should('be.visible')
    }

    verifyErrAll(){
        cy.contains(this.err_all).should('be.visible')
    }

    verifyErr(){
        cy.contains(this.err).should('be.visible')
    }

    submitOtherResponse(){
        cy.contains(this.success).click()
    }

    verifySubmitOtherResponse(){
        cy.contains(this.verify_success).should('be.visible')
    }
    

    

}
export default new addForm()